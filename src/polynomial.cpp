#include <trajectory_generator/polynomial.h>

Polynomial::Polynomial(ros::NodeHandle& nh)
    : nh_(nh),
      kin_(),
      robot_name_("mini"),
      z_offset_(0.4),
      max_v_(0.1),
      max_a_(0.1),
      max_ang_v_(0.1),
      max_ang_a_(0.1),
      max_j_v_(0.1),
      max_j_a_(0.1),
      max_wheel_velocity_(3.4){
    
  // create publisher for RVIZ markers
  pub_markers_ =
      nh.advertise<visualization_msgs::MarkerArray>("trajectory_markers", 0);
  pub_path_ = nh.advertise<nav_msgs::Path>("/path_visualize", 0);

  // Publishes points individually
  pub_command_ = nh_.advertise<trajectory_msgs::MultiDOFJointTrajectory>(
      "/command/trajectory", 1);

  // Subscriber to waypoints
  sub_waypoints_ = nh_.subscribe("/waypoints", 1, &Polynomial::waypointCallback, this);

  nh_.param("/robot_name", robot_name_, robot_name_);
  kin_.setKinematicParameters(robot_name_);
  nh_.param("/max_wheel_velocity", max_wheel_velocity_, max_wheel_velocity_);
  nh_.param("/sample_time", traj_sampler_.dt_, traj_sampler_.dt_);
  ROS_INFO("Trajectory sample time: %.3f s", traj_sampler_.dt_);

  const bool oneshot = false;
  const bool autostart = false;
  publish_timer_ = nh_.createTimer(ros::Duration(traj_sampler_.dt_),
                                   &Polynomial::commandTimerCallback,
                                   this, oneshot, autostart);
}

Polynomial::~Polynomial(){ publish_timer_.stop(); }

void Polynomial::setMaxSpeed(const double max_v, const double max_w) 
{
  max_v_ = max_v;
  max_ang_v_ = max_w;
}

void Polynomial::waypointCallback(
  const trajectory_msgs::JointTrajectory::ConstPtr& msg)
{
  ROS_INFO("New waypoint trajectory received!");
  
  int ndim = msg->points[0].positions.size();

  // Define waypoints
  std::vector<Eigen::VectorXd> waypoints;
  Eigen::VectorXd tseg(msg->points.size());
  Eigen::VectorXd waypoint(ndim);

  // Iterate through all waypoints
  for (int i=0; i<msg->points.size(); i++)
  {
    // std::cout << "waypoint: " << i " " << msg->points[i].positions[0] << std::endl;
    for (int j=0; j<ndim; j++)
      waypoint(j) = msg->points[i].positions[j];
    waypoints.push_back(waypoint);
    tseg(i) = msg->points[i].time_from_start.toSec();
  }
  
  // Display waypoint info
  // for (int i=0; i<msg->points.size(); i++)
  //   std::cout << "Point " << i << " : " << waypoints[i].transpose() << std::endl;
  // std::cout << "Tint: " << tseg.transpose() << std::endl;
  
  // Clear trajectory
  trajectory_.clear();

  // Set trajectory axis name
  for (int i=0; i<msg->joint_names.size(); i++)
    trajectory_.axis_name.push_back(msg->joint_names[i]);

  // Estimate segment times
  estimateSegmentTimes(waypoints, tseg);
  
  // Generate trajectory
  if (!planTrajectory(waypoints, tseg, &trajectory_))
  {
    ROS_WARN("Trajectory generation failed!");
    return;
  }
  
  // Check trajectory feasibility
  if (!checkFeasibility(trajectory_))
  {
    ROS_WARN("Trajectory generation failed!");
    return;
  }
  
  ROS_INFO("Trajectory generation success!");
  publishVisualTrajectory();

  // checkRange(trajectory);

  // Trajectory Info
  std::cout << "Trajectory time: " << trajectory_.getMaxTime() << std::endl;
  // std::cout << trajectory_.getSize() << std::endl;
  // Start sampling trajectory
  publish_timer_.start();
  // start_time_ = ros::Time::now();
  traj_sampler_.current_time_index_ = 0;
  traj_sampler_.final_trigger_ = false;
}

void Polynomial::estimateSegmentTimes(
  const std::vector<Eigen::VectorXd>& waypoints, Eigen::VectorXd& tseg)
{
  double magic_fabian_constant = 6.5;
  double tscale = 1.1;
  double distance;

  // Buffer for each part
  Eigen::VectorXd t1(waypoints.size());
  Eigen::VectorXd t2(waypoints.size());
  Eigen::VectorXd t3(waypoints.size());

  // Resize all buffers
  tseg.resize(waypoints.size());
  t1.setZero();
  t2.setZero();
  t3.setZero();
  
  for (int i=1; i<waypoints.size(); i++)
  {
    // XY motion
    distance = (waypoints[i].head(2) - waypoints[i-1].head(2)).norm();
    t1(i) = distance / max_v_ * tscale * (1.0 + magic_fabian_constant * max_v_ / max_a_ *
                                exp(-distance / max_v_ * 2));
    
    // Yaw motion
    distance = abs(waypoints[i](3) - waypoints[i-1](3));
    t2(i) = distance / max_ang_v_ * tscale * (1.0 + magic_fabian_constant * max_ang_v_ / max_ang_a_ *
                                exp(-distance / max_ang_v_ * 2));
    
    // Leg joints
    Eigen::Vector2d jdiff = (waypoints[i].tail(2) - waypoints[i-1].tail(2)).cwiseAbs();
    distance = (jdiff(0) > jdiff(1)) ? jdiff(0) : jdiff(1);
    t3(i) = distance / max_j_v_ * tscale * (1.0 + magic_fabian_constant * max_j_v_ / max_j_a_ *
                                exp(-distance / max_j_v_ * 2));
  }
  // Gets the max from each
  tseg = t1.cwiseMax(t2.cwiseMax(t3));
  // Increment the time segment by adding with previous
  for (int i=1; i<waypoints.size(); i++){
    tseg(i) += tseg(i-1);
    tseg(i) = ceil(tseg(i));
  }

  // std::cout << "t1: " << t1.transpose() << std::endl;
  // std::cout << "t2: " << t2.transpose() << std::endl;
  // std::cout << "t3: " << t3.transpose() << std::endl;
  std::cout << "Tseg: " << tseg.transpose() << std::endl;
}
// Plans a trajectory from a start position and velocity to a goal position and velocity
bool Polynomial::planTrajectory(const std::vector<Eigen::VectorXd>& waypoints,
                                const Eigen::VectorXd tseg,
                                Trajectory* trajectory) 
{
  assert(trajectory);

  // Estimate initial segment times if empty
  if (tseg.size() != waypoints.size())
    printf("Estimating segment time\n");
  
  // Set up bspline class with default params
  BSplineGenerator bspline;

  // Generate trajectory
  bspline.generateSpline(waypoints, tseg, traj_sampler_.dt_, trajectory);

  // Scale trajectory timing to respect constraint
  // Recompute tseg
  // Recompute spline
  // trajectory->scaleSegmentTimesToMeetConstraints(v_max, a_max);
  // std::cout << "bf:" << trajectory->getMaxTime() << std::endl;
  // std::cout << "af:" << trajectory->getMaxTime() << std::endl;
  return true;
}

bool Polynomial::checkFeasibility(const Trajectory& trajectory)
{
  ROS_INFO("Checking trajectory feasibility ...");
  
  int tindex = 0;
  Eigen::MatrixXd v_cmd(5,1);
  Eigen::MatrixXd rot2D(2,2);

  while (tindex < trajectory_.getSize())
  {
    Eigen::VectorXd pos = trajectory_.points[tindex].pos;
    Eigen::VectorXd vel = trajectory_.points[tindex].vel;

    // Transform velocity to robot frame
    double yaw = pos(3);
    rot2D << cos(yaw), sin(yaw), -sin(yaw), cos(yaw);
    v_cmd << rot2D*vel.head(2), vel.tail(3);

    // Update robot kinematics i.e. jacobian
    kin_.updateRobotLinkJacobian(pos(4), pos(5));

    auto w_cmd = kin_.jac_wheel_*v_cmd;
    auto w_abs = w_cmd.cwiseAbs();
    // // std::cout << vel.head(2).transpose() << std::endl;
    // // std::cout << vout.transpose() << std::endl;
    // std::cout << v_cmd.transpose() << std::endl;
    // std::cout << w_cmd.transpose() << std::endl;
    // // std::cout << w_abs.transpose() << std::endl;
    // std::cout << "===========================" << std::endl;
    // std::cout << abs_w.maxCoeff() << std::endl;
    auto w_max = w_abs.maxCoeff();
    if (w_max > max_wheel_velocity_ + 1e-3)
    {
      ROS_WARN("Wheel velocity exceeded %.3f\n", w_max);
      // return false;
    }

    tindex += 1;
  }
  ROS_INFO("Trajectory feasibile!");
  
  return true;
}

void Polynomial::commandTimerCallback(const ros::TimerEvent&)
{
  static int empty_count = 0;
  int sample_tindex = 0;

  // std::cout << traj_sampler_.current_time_index_ << "  " << trajectory_.getMaxTime() << std::endl;
  
  if (traj_sampler_.final_trigger_ == false)
  {
    // Publish first and last point - dragging it for a bit
    if (empty_count==0)
      ROS_INFO("Trajectory Sampler: Dragging setpoint ...");
    
    // Setpoint is either first or last time for fixed amount of time
    if (traj_sampler_.current_time_index_ == 0)
      sample_tindex = 0;
    else if (traj_sampler_.current_time_index_ == trajectory_.getSize())
      sample_tindex = trajectory_.getSize() - 1;

    trajectory_msgs::MultiDOFJointTrajectory msg;
    trajectoryToMsg(sample_tindex, msg);
    pub_command_.publish(msg);

    // Increment counter & reset counter and flags when finished
    empty_count++;
    // std::cout << empty_count << " " << sample_tindex << std::endl;
    if (empty_count > int(1/traj_sampler_.dt_))
    {
      empty_count = 0;
      traj_sampler_.final_trigger_ = true;
      if (traj_sampler_.current_time_index_ < trajectory_.getSize())
        ROS_INFO("Trajectory Sampler: Publishing trajectory setpoints ...");
    }
  }
  else if (traj_sampler_.current_time_index_ < trajectory_.getSize())
  {
    // std::cout << "Idx: " << traj_sampler_.current_time_index_ << std::endl;
    //           << " " << trajectory_.points[traj_sampler_.current_time_index_].t
    //           << " " << trajectory_.getSize() << std::endl; 
    trajectory_msgs::MultiDOFJointTrajectory msg;
    trajectoryToMsg(traj_sampler_.current_time_index_, msg);
    pub_command_.publish(msg);

    traj_sampler_.current_time_index_ += 1;

    // Trigger end setpoint  
    if (traj_sampler_.current_time_index_ >= trajectory_.getSize())
      traj_sampler_.final_trigger_ = false;
  }
  else
  {
    ROS_INFO("Trajectory Sampler: Finished!");
    publish_timer_.stop();
  }
}

void Polynomial::trajectoryToMsg(int tindex, trajectory_msgs::MultiDOFJointTrajectory& msg)
{
  msg.header.stamp = ros::Time::now();
  msg.joint_names.push_back("base_link");
  msg.joint_names.push_back("j5");
  msg.joint_names.push_back("j6");
  
  // std::cout << trajectory_.points[tindex].pos.transpose() << std::endl;
  Eigen::Vector3d zero = Eigen::Vector3d::Zero();
  Eigen::VectorXd pos = trajectory_.points[tindex].pos;
  Eigen::VectorXd vel = trajectory_.points[tindex].vel;
  Eigen::VectorXd acc = trajectory_.points[tindex].acc;

  trajectory_msgs::MultiDOFJointTrajectoryPoint point;

  geometry_msgs::Transform position;
  geometry_msgs::Twist velocity;
  geometry_msgs::Twist acceleration;

  // ============ Base Link ============ //
  // for (int i=0; i<trajectory_.axis_name.size(); i++)
  // {
  //   if (trajectory_.axis_name == "x")
  //   {
  //     position.translation.x = pos(i);
  //     velocity.linear.x = vel(i);
  //     acceleration.linear.x = acc(i);
  //   }
  // } 
  position.translation.x = pos(0);
  position.translation.y = pos(1);
  position.translation.z = pos(2);
  tf2::Quaternion quat;
  quat.setRPY( 0, 0, pos(3) );
  quat.normalize();
  tf2::convert(quat, position.rotation);

  velocity.linear.x = vel(0);
  velocity.linear.y = vel(1);
  velocity.linear.z = vel(2);
  velocity.angular.x = 0.0;
  velocity.angular.y = 0.0;
  velocity.angular.z = vel(3);

  acceleration.linear.x = acc(0);
  acceleration.linear.y = acc(1);
  acceleration.linear.z = acc(2);
  acceleration.angular.x = 0.0;
  acceleration.angular.y = 0.0;
  acceleration.angular.z = acc(3);

  point.transforms.push_back(position);
  point.velocities.push_back(velocity);
  point.accelerations.push_back(acceleration);

  // ============ J5 joint ============ //
  position.translation.x = pos(4);
  position.translation.y = 0.0;
  position.translation.z = 0.0;
  quat.setRPY( 0, 0, 0 );
  quat.normalize();
  tf2::convert(quat, position.rotation);

  velocity.linear.x = vel(4);
  velocity.linear.y = 0.0;
  velocity.linear.z = 0.0;
  velocity.angular.x = 0.0;
  velocity.angular.y = 0.0;
  velocity.angular.z = 0.0;

  acceleration.linear.x = acc(4);
  acceleration.linear.y = 0.0;
  acceleration.linear.z = 0.0;
  acceleration.angular.x = 0.0;
  acceleration.angular.y = 0.0;
  acceleration.angular.z = 0.0;

  point.transforms.push_back(position);
  point.velocities.push_back(velocity);
  point.accelerations.push_back(acceleration);

  // ============ J6 joint ============ //
  position.translation.x = pos(5);
  position.translation.y = 0.0;
  position.translation.z = 0.0;
  quat.setRPY( 0, 0, 0 );
  quat.normalize();
  tf2::convert(quat, position.rotation);
  
  velocity.linear.x = vel(5);
  velocity.linear.y = 0.0;
  velocity.linear.z = 0.0;
  velocity.angular.x = 0.0;
  velocity.angular.y = 0.0;
  velocity.angular.z = 0.0;

  acceleration.linear.x = acc(5);
  acceleration.linear.y = 0.0;
  acceleration.linear.z = 0.0;
  acceleration.angular.x = 0.0;
  acceleration.angular.y = 0.0;
  acceleration.angular.z = 0.0;

  point.transforms.push_back(position);
  point.velocities.push_back(velocity);
  point.accelerations.push_back(acceleration);

  msg.points.push_back(point);
  msg.points[0].time_from_start = ros::Duration(trajectory_.points[tindex].t);
}

bool Polynomial::publishVisualTrajectory(){
  // send trajectory as markers to display them in RVIZ
  visualization_msgs::MarkerArray markers;
  // Distance by which to seperate additional markers. Set 0.0 to disable.
  double distance = 0.15; 
  std::string frame_id = "world";

  clearMarkers();
  visualizeTrajectory(distance, frame_id, &markers);
  pub_markers_.publish(markers);

  return true;
}

bool Polynomial::clearMarkers()
{
  visualization_msgs::MarkerArray marker_array;
  visualization_msgs::Marker mark;
  mark.header.frame_id = "world";
  mark.type = visualization_msgs::Marker::ARROW;
  mark.action = visualization_msgs::Marker::DELETEALL;
  marker_array.markers.push_back(mark);
  pub_markers_.publish(marker_array);
  rviz_visual_tools::RvizVisualTools rviz_interface("dummy_wall","/trajectory_markers");
  rviz_interface.deleteAllMarkers();
}

bool Polynomial::visualizeTrajectory(
  double distance, std::string frame_id,
  visualization_msgs::MarkerArray* marker_array)
{
  nav_msgs::Path path;
  path.header.frame_id = frame_id;

  // CHECK_NOTNULL(marker_array);
  marker_array->markers.clear();

  visualization_msgs::Marker line_strip;
  line_strip.header.frame_id = frame_id;
  line_strip.type = visualization_msgs::Marker::LINE_STRIP;
  line_strip.color.r = 1.0;
  line_strip.color.g = 0.65;
  line_strip.color.b = 0.0;
  line_strip.color.a = 1.0;
  line_strip.scale.x = 0.02;
  line_strip.ns = "path";

  visualization_msgs::Marker arrows;
  arrows.header.frame_id = frame_id;
  arrows.type = visualization_msgs::Marker::ARROW;
  arrows.color.r = 1.0;
  arrows.color.g = 0.0;
  arrows.color.b = 0.0;
  arrows.color.a = 1.0;
  arrows.scale.x = 0.1;
  arrows.scale.y = 0.02;
  arrows.scale.z = 0.02;
  arrows.ns = "heading";

  double accumulated_distance = 0.0;
  int marker_id = 0;
  Eigen::Vector3d last_position;// = Eigen::Vector3d::Zero();
  last_position << -100, -100, -100;

  for (int i = 0; i < trajectory_.getSize(); i++) 
  {
    Eigen::Vector3d pos = trajectory_.points[i].pos.head(3);
    
    // Force last position to have marker
    if (i == trajectory_.getSize()-1)
      accumulated_distance = distance + 0.1;
    else  
      accumulated_distance = (pos - last_position).norm();
    // printf("Adist: %.3f\n", accumulated_distance);
    if (accumulated_distance > distance)
    {
      // Resets distance and update reference position
      accumulated_distance = 0.0;
      last_position = pos;

      // Set orientation axes
      tf2::Quaternion quat;
      quat.setRPY( 0, 0, trajectory_.points[i].pos(3) );
      quat.normalize();
      tf2::convert(quat, arrows.pose.orientation);
      arrows.pose.position.x = last_position(0);
      arrows.pose.position.y = last_position(1);
      arrows.pose.position.z = last_position(2);
      arrows.id = marker_id;
      marker_array->markers.push_back(arrows);
      
      // Set line strip (geometric xyz path)
      geometry_msgs::Point last_position_msg;
      last_position_msg.x = last_position(0);
      last_position_msg.y = last_position(1);
      last_position_msg.z = last_position(2);
      // tf::pointEigenToMsg(last_position, last_position_msg);
      line_strip.points.push_back(last_position_msg);
      marker_array->markers.push_back(line_strip);

      marker_id++;

      geometry_msgs::PoseStamped pose;
      pose.header.frame_id = frame_id;
      pose.pose.position.x = last_position(0);
      pose.pose.position.y = last_position(1);
      pose.pose.position.z = last_position(2);
      tf2::convert(quat, pose.pose.orientation);
      path.poses.push_back(pose);
    }
  }
  pub_path_.publish(path);
}
/*
// TEMP FUNCTION FOR EXPERIMENTS
void Polynomial::checkRange(
  const mav_trajectory_generation::Trajectory& trajectory)
{
  // List of joint angles to assess
  Eigen::MatrixXd joint_config(18,2);
  joint_config << 0, 0,
                  20, -20,
                  64, -64,
                  90, -90,
                  107, -107,
                  126, -127,
                  180, -180,
                  45, -63,
                  5, -40,
                  40, -5,
                  0, -179,
                  179, 0,
                  65, -120,
                  120, -65,
                  115, -160,
                  160, -115,
                  161, -171,
                  171, -161;

  for (int i=0; i<joint_config.rows(); i++)
  {
    std::cout << "q: " << joint_config.row(i) << std::endl;
    double j5 = joint_config(i,0) * M_PI/180.0;
    double j6 = joint_config(i,1) * M_PI/180.0;
    kin_.updateKinematics(j5,j6);
    checkFeasibility(trajectory);
  }  
}
*/

// void Polynomial::test()
// {
// 	std::vector<Eigen::VectorXd> waypoints;
// 	Eigen::VectorXd tseg;
// 	Eigen::VectorXd point(5,1);

// 	point << 0.23, 0.0, 0.0, 0.0, 0.01;
// 	waypoints.push_back(point);
// 	point << 1.0, 1.0, 0.0, 0.1, -0.4;
// 	waypoints.push_back(point);
// 	point << 1.1, 1.2, 0.0, 0.5, -0.8;
// 	waypoints.push_back(point);
// 	// point << 2.0, 0.0, 0.0, 0.0, -1.2;
// 	// waypoints.push_back(point);

// 	tseg.resize(waypoints.size());
//   tseg << 0.0, 0.4, 0.7;

//   trajectory_msgs::JointTrajectory trajectory;
//   trajectory_msgs::JointTrajectoryPoint tpoint;
//   tpoint.positions.resize(waypoints[0].size());
//   for (int i=0; i<waypoints.size(); i++)
//   {
//     // tpoint.positions = [waypoints[i](0), waypoints[i](1)];
//     for(int j=0; j<waypoints[0].size(); j++) {
//         tpoint.positions[i] = waypoints[i](j);
//     }
//     trajectory.points.push_back(tpoint);
    
//     // std::cout << trajectory.points[i].positions << std::endl;
//   }
//   // trajectory_msgs::JointTrajectory::ConstPtr msg_p(new trajectory_msgs::JointTrajectory(trajectory));
//   // waypointCallback(msg_p);
// }
