#include <iostream>
#include <Eigen/Dense>
#include <math.h> 
#include <queue>

#include "trajectory_generator/bspline_generator.h"

BSplineGenerator::BSplineGenerator() :
  s_order_(4),
	s_degree_(2),
	s_dim_(0),
	n_knot_(0)
{}

BSplineGenerator::~BSplineGenerator() {}

void BSplineGenerator::knotFunction()
{
	U_.resize(t_.size()+2*s_order_+1);
  
	for (int i=0; i<t_.size()+2*s_order_+1; i++)
	{
		if (i <= s_order_)
			U_(i) = t_(0);
		else if (i >= s_order_ && i<t_.size()+s_order_)
			U_(i) = (t_(i-s_order_)+t_(i-s_order_-1))/2.0;
		else
			U_(i) = t_(t_.size()-1);
	}
	
	n_knot_ = U_.size() - 1;
  // std::cout << U_.transpose() << std::endl;
	// std::cout << U_.size() << " " << n_knot_ << std::endl;
}


int BSplineGenerator::whichSpan(double u)
{
	// """ Compute i for a given u """
	if (abs(u-U_(U_.size()-1)) < 1e-5)
	{
		u -= 0.001;
		// std::cout << U_.transpose() << std::endl;
		// std::cout << U_(U_.size()-1) << std::endl;
		// std::cout << "C1: " << u << std::endl;
	}

	int high = n_knot_ - s_order_;
	int low = s_order_;
	int mid = 0;
	if (u == U_(high))
		mid = high;
	else
		mid = (high+low)/2;
		while ((u<U_(mid)) || (u>=U_(mid+1)))
		{
			// print u, U[mid], U[mid+1]
			if (u==U_(mid+1))
				mid = mid+1; 		// knot with multiplicity >1
			else
				if (u > U_(mid))
					low = mid;
				else
					high=mid;
				mid = (high+low)/2;
		}
	return mid;
}

void BSplineGenerator::DersBasisFunction(
	double u, int i, int n, Eigen::MatrixXd& Ders)
{
	// ## Define Variables ##
	double temp = 0.0;
  int r = 0;

	int MAX_P = 6;
	int matrix_size = 5;
	Eigen::VectorXd DR(MAX_P+1);
	Eigen::VectorXd DL(MAX_P+1);
	Eigen::MatrixXd Du(matrix_size,matrix_size);
	Eigen::MatrixXd a(matrix_size,matrix_size);
	Ders.resize(3,matrix_size);

	DR.setZero();
	DL.setZero();
	Du.setZero();
	a.setZero();
	Ders.setZero();
	
	Du(0,0) = 1.0;

	for (int j=1; j<s_order_+1; j++)
	{
		DL(j) = u - U_(i+1-j);
		DR(j) = U_(i+j)-u;
		double acc = 0.0;
		for (int r=0; r<j; r++)
		{
			Du(j,r) = DR(r+1) + DL(j-r);
			if (abs(Du(j,r)) < 1e-5)
			{
				temp = 0.0;
				std::cout << Du(j,r) << std::endl;
			}
			else  
				temp = Du(r,j-1) / Du(j,r);
			Du(r,j) = acc + DR(r+1) * temp;
			acc = DL(j-r) * temp;
		}

		Du(j,j) = acc;
  }
	
	for (int j=0; j<s_order_+1; j++)
		Ders(0,j) = Du(j,s_order_);

	// std::cout << DL.transpose() << std::endl;
	// std::cout << DR.transpose() << std::endl;
	// std::cout << Du << std::endl;
	// std::cout << Ders << std::endl;
  // std::cout << "===========" << std::endl;
	// Derivatives of basis functions
	for (int r=0; r<s_order_+1; r++)
	{
		int s1 = 0;
		int s2 = 1;
		int j1 = 0;
		int j2 = 0;
		
		a(0,0) = 1.0;

		for (int k=1; k<n+1; k++)
		{
			double d = 0.0;
			int rk = r - k;
			int pk = s_order_ - k;

			if (r >= k)
			{
				if (abs(Du(pk+1,rk)) < 1e-5)
				{std::cout << "zero" << Du(pk+1,rk) << std::endl;
				  a(s2,0) = 0.0;}
				else
				  a(s2,0) = a(s1,0) / Du(pk+1,rk);
					// printf("a0 %.3f \n", a(s2,0));
				d = a(s2,0) * Du(rk,pk);
			}

			if (rk >= -1)
				j1 = 1;
			else
				j1 = -rk;

			if (r-1 <= pk)
				j2 = k - 1;
			else
				j2 = s_order_ - r;

			for (int j=j1; j<j2+1; j++)
			{
				if (abs(Du(pk+1,rk+j)) < 1e-5)
				{std::cout << "zero" << Du(pk+1,rk+j) << std::endl;
				  a(s2,j) = 0.0;}
				else
					a(s2,j) = ((a(s1,j) - a(s1,j-1))) / Du(pk+1,rk+j);
				d += a(s2,j) * Du(rk+j,pk);
				// printf("d1 %.3f \n", d);
			}
			
			if (r <= pk)
			{
				// std::cout << r << " " << pk << std::endl;
				// printf("d2 prior %.3f \n", d);
				if (abs(Du(pk+1,r))<1e-5)
				{std::cout << "zero" << Du(pk+1,r) << std::endl;
				  a(s2,k) = 0.0;}
				else
				  a(s2,k) = -a(s1,k-1) / Du(pk+1,r);
				d += a(s2,k) * Du(r,pk);
				// printf("d2 %.3f \n", d);
			}

			Ders(k,r) = d;
			int j = s1; 
			s1 = s2; 
			s2 = j;
			// printf("d %.4f\n ================== \n", d);
		}	
	}
	r = s_order_;
  // std::cout << a << std::endl;
	for (int k=1; k<n+1; k++)
	{
		for (int j=0; j<s_order_+1; j++)
			Ders(k,j) *= r;
		r *= (s_order_-k);
	}
  
	// std::cout << Ders << std::endl;
}

void BSplineGenerator::controlPoint()
{
	// Calculate control points
  Eigen::MatrixXd A;
	A.resize(n_knot_-s_order_, n_knot_-s_order_);
	A.setZero();
	
	int	row_counter = 0;
	int z_max = t_.size();
	
	int n = 0;
	int offset = 0;
	for (int z=0; z<z_max; z++)
	{
		double u;
		// check to prevent u = umax
		if (U_(U_.size()-1) == t_(z))
			u = t_(z) - 0.001;
		else
			u = t_(z);
		
		int i = whichSpan(u); // compute i
		// std::cout << z << " " << u << " " << i << std::endl;
		
		// Order of derivatives & offset for stacking
		if (z==0.0 || z==z_max-1)
		{
			n = 2;
			offset = 3;
		}
		else
		{
			n = 0;
			offset = 1;
		}

		// compute control point
		Eigen::MatrixXd derv;
		DersBasisFunction(u, i, n, derv);
		// std::cout << derv << std::endl;
		// std::cout << "========================" << std::endl;
			
		// Stack into matrix A
		if (z==z_max-1)
		{
			for (int y=offset-1; y>0; y--)
			{
				for (int q=0; q<5; q++)
				  A(row_counter,z+q) = derv(y,q);
				row_counter+=1;
			}
		}
		else
		{
			for (int y=0; y<offset; y++)
			{
				for (int q=0; q<5; q++)
					A(row_counter,z+q) = derv(y,q);
				row_counter+=1;
			}
		}
	} // end z_max

	// first and last element always unity
	A(A.rows()-1,A.cols()-1) = 1.0;

	P_ = A.colPivHouseholderQr().solve(C_.transpose());  // Stable, slowest. 
	
	// std::cout << C_ << std::endl;
	// std::cout << A << std::endl;
	// std::cout << x << std::endl;
}

void BSplineGenerator::sampleEntireTrajectory(
	const double ts, Trajectory* trajectory)
{
	// 	Evaluate spline
	int idx = 0;
	Eigen::MatrixXd A;
	A.setZero();
  Point p(s_dim_);
  // std::cout << P_ << std::endl;

  double tint = 0.0;

	while (tint < t_(t_.size()-1) ||
	    abs(tint - t_(t_.size()-1)) < ts/2 )
	{
		// std::cout << tint << std::endl;
		idx = whichSpan(tint);
		DersBasisFunction(tint, idx, 2, A);

		p.setZero();
		for (int k=0; k<s_dim_; k++)
		{
			for (int j=0; j<s_order_+1; j++)
			{
				// std::cout << idx-s_order_+j << " " << k << std::endl;
				// std::cout << P_(idx-s_order_+j,k) << " " << A(0,j) << std::endl;
				p.pos(k) += P_(idx-s_order_+j,k)*A(0,j);
				p.vel(k) += P_(idx-s_order_+j,k)*A(1,j);
				p.acc(k) += P_(idx-s_order_+j,k)*A(2,j);
			}
			// std::cout << tint << "\t" << p.pos(k) << std::endl;
	  }
		p.t = tint;
		trajectory->points.push_back(p);

		tint += ts;
	}

	
	// for (int i=0; i<trajectory->points.size(); i++)
	// {
	// 	std::cout << trajectory->points[i].t << "\t" << trajectory->points[i].pos.transpose() << std::endl;
	// }
}

void BSplineGenerator::pointInterpolation(
	const std::vector<Eigen::VectorXd>& waypoints)
{ 
	// expand input array size for bspline computation """

	s_dim_ = waypoints[0].size();
	
	C_.resize(s_dim_, waypoints.size()+4);
  // std::cout << waypoints.size()+4-s_order_+1 << std::endl;
	// Put first and last item
	C_.leftCols(1) = waypoints[0];
	C_.rightCols(1) = waypoints.back();
  // std::cout << C_<< std::endl;

	// Put intermediate points, gap depends on spline order
	for (int i=0; i<s_dim_; i++)
	{
		int k = 0;
		for (int j=s_order_-1; j<waypoints.size()+4-s_order_+1; j++)
		{
			C_(i,j) = waypoints[j-(s_order_-2)](i);
			k += 1;
		}
	}
	// std::cout << C_<< std::endl;
}

// def computeTimeIntervals(self, q):
// 	""" Compute time interval for spline if not specified 	"""
// 	""" Intervals are at unit time (1)						"""

// 	return np.arange(0,len(q),1)

bool BSplineGenerator::generateSpline(
	const std::vector<Eigen::VectorXd>& waypoints,
	const Eigen::VectorXd& tseg, const double ts,
	Trajectory* trajectory)
{
	// Remap for convenience
	t_ = tseg;
	
	// Append points accordingly
	pointInterpolation(waypoints);
	
	// Determine U, n_knot
	knotFunction(); 
	
	// Calculate control points
	controlPoint();

	// Samples trajectory
	sampleEntireTrajectory(ts, trajectory);
	
	return true;
}

void BSplineGenerator::test()
{
	std::vector<Eigen::VectorXd> waypoints;
	Eigen::VectorXd tseg;
	Eigen::VectorXd point(5,1);
	double sample_time = 0.1;

	point << 0.23, 0.0, 0.0, 0.0, 0.01;
	waypoints.push_back(point);
	point << 1.0, 1.0, 0.0, 0.1, -0.4;
	waypoints.push_back(point);
	point << 1.1, 1.2, 0.0, 0.5, -0.8;
	waypoints.push_back(point);
	// point << 2.0, 0.0, 0.0, 0.0, -1.2;
	// waypoints.push_back(point);

	tseg.resize(waypoints.size());
  tseg << 0.0, 0.4, 0.7;

	Trajectory trajectory;
	generateSpline(waypoints, tseg, sample_time, &trajectory);
}
