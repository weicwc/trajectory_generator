#!/usr/bin/env python
""" Simple Roll PID controller """

import rospy
import math
import numpy as np
from trajectory_msgs.msg import JointTrajectory
from trajectory_msgs.msg import JointTrajectoryPoint
from trajectory_msgs.msg import MultiDOFJointTrajectory

from pspline_generator import SplineGenerator as Pspline
import plotgraph as Plot

class TrajectoryGenerator:
  def __init__(self,rate):
    rospy.init_node('TrajectoryGenerator') 		# Initialises node
    self.rate 	  = rospy.Rate(rate)	    # Controller rat

    self.pub_cmd_ = rospy.Publisher('/command/trajectory', MultiDOFJointTrajectory, queue_size=10)
    self.sub_traj_ = rospy.Subscriber('/experiment/trajectory', JointTrajectory, self.waypointCallback, queue_size=10)
    self.controller_timer_ = rospy.Timer(rospy.Duration(1.0/rate), self.timerCallback)
    self.debug = True

    self.spline_gen_ = Pspline()

  def waypointCallback(self):
    
    generateTrajectory()
    
  def timerCallback(self, timer):
    
    return
    

  def generateTrajectory(self):
    t_com = np.array([0.0, 0.4, 0.5, 1.0]) 
    x_com = np.array([0.0, 0.0, 0.0])
    x_com = np.vstack((x_com, np.array([1.0, 1.0, 0.0])))
    x_com = np.vstack((x_com, np.array([1.1, 1.1, 0.0])))
    x_com = np.vstack((x_com, np.array([2.0, 0.0, 0.0])))

    ct,cp,cv,ca = self.spline_gen_.generate_spline(x_com, t_com, 0.05)
    Plot.plot_2d(ct, cv)

if __name__ == "__main__":

  print 'Initialising TrajectoryGenerator ....'
  manager = TrajectoryGenerator(100)
  rospy.loginfo('TrajectoryGenerator Ready!')

  manager.generateTrajectory()