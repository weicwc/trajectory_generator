#!/usr/bin/env python

import sys; sys.dont_write_bytecode = True

import plotgraph as Plot
import time
import numpy as np
from scipy import linalg
from scipy import arange

class SplineGenerator:
	def __init__(self):
		self.p  = 4; 			# order of spline
		self.n  = 2; 			# derivative degree
		self.ndim = 0			# spline dimension
		self.C  = np.matrix([0])	# matrix of via points
		self.t0 = np.array([0.0])	# array of time
		self.U  = np.array([0.0])
		self.n_knot = 0

	def knotFunction(self):
		""" Determine knots required """
		
		self.U = np.zeros(len(self.t0)+2*self.p+1)
		
		for i in range(0,len(self.t0)+2*self.p+1):
			if (i<=self.p):
				self.U[i] = self.t0.item(0) 
			elif (i>=self.p and i<len(self.t0)+self.p):
				self.U[i] = (self.t0.item(i-self.p)+self.t0.item(i-self.p-1))/2.0 
			else:
				self.U[i] = self.t0.item(-1)

		n_knot = len(self.U) - 1
		# print self.U, len(self.U), n_knot
		return self.U, n_knot

	def whichSpan(self, u, U, n_knot, p):
		""" Compute i for a given u """

		if (u==U[-1]):
			u = u - 0.001

		high = n_knot - p;
		low = p;
		if (u == U[high]):
			mid = high;
		else:
			mid = (high+low)/2;
			while ((u<U[mid]) or (u>=U[mid+1])):
				# print u, U[mid], U[mid+1]
				if (u==U[mid+1]):
					mid = mid+1; 			# knot with multiplicity >1 */
				else:
					if (u > U[mid]):
						low = mid;
					else:
						high=mid;
					mid = (high+low)/2;
		return mid

	def DersBasisFunction(self, u, U, p, i, n=2):
		""" Basis functions with derivatives """

		## Define Variables ##
		MAX_P = 6
		matrix_size = 5
		DR = np.zeros(MAX_P+1)
		DL = np.zeros(MAX_P+1)
		Du = np.zeros((matrix_size,matrix_size))
		a  = np.zeros((matrix_size,matrix_size))
		Ders = np.zeros((3,matrix_size))
		
		Du[0][0] = 1.0;

		for j in range(1,p+1):

			DL[j] = u - U[i+1-j];
			DR[j] = U[i+j]-u;
			acc = 0.0;
			for r in range(0,j):
				Du[j][r] = DR[r+1] + DL[j-r];
				try:
					temp = float(Du[r][j-1]) / float(Du[j][r]);
				except ZeroDivisionError:
					temp = 0.0
				Du[r][j] = acc + DR[r+1] * temp;
				acc = DL[j-r] * temp;

			Du[j][j] = acc;

		for j in range(0,p+1):
			Ders[0][j] = Du[j][p];

		# print np.round(DL,3)
		# print np.round(DR,3)
		# print np.round(Du,3)
		# print np.round(Ders,3)
		# print "==========================="
		## Derivatives of basis functions
		for r in range(0,p+1):

			s1=0;
			s2=1;
			a[0][0] = 1.0;
			for k in range(1,n+1):

				d = 0.0;
				rk = r - k;
				pk = p - k;
				if (r >= k):
					try:
						a[s2][0] = float(a[s1][0]) / float(Du[pk+1][rk]);
						# print 'a0: ', a[s2][0]
					except ZeroDivisionError:
						a[s2][0] = 0.0
						print 'zero: ', float(Du[pk+1][rk]);
					d = a[s2][0] * Du[rk][pk];
					# print "d0: ", np.round(d,3)

				if (rk >= -1):
					j1 = 1;
				else:
					j1 = -rk;

				if (r-1 <= pk):
					j2 = k - 1;
				else:
					j2 = p - r;

				for j in range(j1,j2+1):
					try:
						a[s2][j] = float((a[s1][j] - a[s1][j-1])) / float(Du[pk+1][rk+j]);
					except ZeroDivisionError:
						a[s2][j] = 0.0
						print 'zero: ', float(Du[pk+1][rk+j]);
					d += a[s2][j] * Du[rk+j][pk];
					# print 'd1: ', d
					
				if (r <= pk):
					# print r, pk
					try:
						a[s2][k] = float(-a[s1][k-1]) / float(Du[pk+1][r]);
					except ZeroDivisionError:
						a[s2][k] = 0.0
						print 'zero: ', float(Du[pk+1][r])
					# print 'd2 prior: ', d
					d += a[s2][k] * Du[r][pk];
					# print 'd2: ', d

				Ders[k][r] = d;
				# print np.round(d,4)
				# print '===================='
				j = s1; s1 = s2; s2 = j;
			
		r = p;
		# print a
		for k in range(1,n+1):
			for j in range(0,p+1):
				Ders[k][j] *= r;
			r *= (p-k);

		return Ders

	def controlPoint(self, U, n_knot, dim=1):
		""" Calculate control points """

		A = np.zeros((n_knot-self.p,n_knot-self.p))
		row_counter = 0
		z_max = len(self.t0)

		for z in range(0,z_max):
			# check to prevent u = umax
			if (U[-1]==self.t0[z]):		
				u = self.t0[z] - 0.001	
			else:
				u = self.t0[z]
			
			i = self.whichSpan(u, U, n_knot, self.p) 		# compute i
			# print z, u, i
			# order of derivatives & offset for stacking
			if (z==0.0 or z==z_max-1):
				n = 2;	offset = 3; 			
			else:
				n = 0;	offset = 1;

			temp = self.DersBasisFunction(u, U, self.p, i, n) 	# compute control point
			# print np.round(temp,3)
			# print '============================'
			# Stack into matrix A
			if (z==z_max-1):
				for y in range(offset-1,0,-1):
					for q in range(0,5):
						A[row_counter][z+q] = temp[y][q]
					
					row_counter+=1
			else:
				for y in range(0,offset):
					for q in range(0,5):
						A[row_counter][z+q] = temp[y][q]
					
					row_counter+=1

		A[-1][-1] = 1.0 	# first and last element always unity
		
		# print A.shape
		# for i in range(0,A.shape[1]):
		# 	print np.round(A[i],3)
		try:
			P = linalg.solve(A,self.C.getT(),check_finite=False) 		# control points
		except: 
			print 'no points found!'
		return P
	
	def evaluateSpline(self, u, P, n_knot, dim=1):
		""" Spline function for given time t """

		sp  = []
		sv  = []
		sa  = []

		# print 'going into whichSpan ', u
		i = self.whichSpan(u, self.U, n_knot, self.p);
		# print 'going into basis func ', u
		A =	self.DersBasisFunction(u, self.U, self.p, i)
		
		# Iterate through each spline dimension
		for k in range(0, self.ndim):
			sp.append(0)
			sv.append(0)
			sa.append(0)
			# print sp[k]
			# print '================='
			for j in range(0,self.p+1):
				sp[k] = sp[k] + P[i-self.p+j][k]*A[0][j]
				sv[k] = sv[k] + P[i-self.p+j][k]*A[1][j]
				sa[k] = sa[k] + P[i-self.p+j][k]*A[2][j]
			# if (k==0):
			# 	print sp[k]
		return sp, sv, sa

	def splineGeneration(self, x, t, tn):
		""" computes b-spline given via points and time interval 	"""
		""" Input:  a) C  -> 2D array - positions (x,y,z) 					
			 		b) t  -> Time interval for each via point
					c) tn -> Output spline time interval			""" 
		""" Ouput: Tuple of 2D list - 
								(time,position,velocity, acceleration)	"""
		self.t0 = t
		self.C  = x
		U, n_knot = self.knotFunction() 		# determine U, n_knot
		P = self.controlPoint(U, n_knot) 			# calculate control point P
		# print np.round(P,3)
		ct = [] 	# output timing array
		cp = [] 	# output position array
		cv = [] 	# output velocity array
		ca = [] 	# output array array
		
		## Evaluate spline
		## ToDo: move this into a seperate function
		for i in np.linspace(0,t[-1],(t[-1]-0)/tn+1):
			t1, t2, t3 = self.evaluateSpline(i, P, n_knot)

			ct.append(i) 
			cp.append(t1) 
			cv.append(t2) 
			ca.append(t3) 
		
		# sp = []
		# sv = []
		# sa = []
		# for i in range(1, self.t0.shape[0]):
		# 	# print self.t0[i]
		# 	a = self.whichSpan(self.t0[i], self.U, n_knot, self.p)
		# 	# print 'going into basis func ', u
		# 	A =	self.DersBasisFunction(self.t0[i], self.U, self.p, a)

		# 	# print 'a ', self.t0[i], a, self.p
		# 	# print np.round(A,3)
		# 	# print '======================'
		# 	# Iterate through each spline dimension
		# 	for k in range(0, 1):
		# 		sp.append(0)
		# 		sv.append(0)
		# 		sa.append(0)
		# 		# print sp[k]
		# 		# print '================='
		# 		for j in range(0,self.p+1):
		# 			# print a-self.p+j, k
		# 			print np.round(P[a-self.p+j][k],3), np.round(A[0][j],3)
		# 			sp[k] = sp[k] + P[a-self.p+j][k]*A[0][j]
		# 			sv[k] = sv[k] + P[a-self.p+j][k]*A[1][j]
		# 			sa[k] = sa[k] + P[a-self.p+j][k]*A[2][j]
		# 		if (k==0):
		# 			print sp[k]
		
		return ct,cp,cv,ca

	def pointInterpolation(self, cpath, ctime):
		""" expand input array size for bspline computation """

		nsize = cpath.shape
		self.ndim = cpath.shape[1]

		C  = np.zeros((self.ndim,len(cpath)+4))
		print C
		# print len(cpath)+4-self.p+1
		# print 'this: ', C.shape[1]-self.p+1
		# print cpath
		for i in range(0,self.ndim):
			k = 0
			for j in range(self.p-1, C.shape[1]-self.p+1):
				print i, j
				C[i][j] = cpath.item(j-(self.p-2),i)
				k += 1
		
		for i in range(0,nsize[1]):
			C[i][0] = cpath[0][i]
			C[i][-1] = cpath[-1][i]
		
		return np.matrix(C)

	def computeTimeIntervals(self, q):
		""" Compute time interval for spline if not specified 	"""
		""" Intervals are at unit time (1)						"""

		return np.arange(0,len(q),1)

	def generateSpline(self, x, t=None, tn=0.1):
		""" 	Compute spline using via points only	"""
		
		## Set t if undefined
		if (t is None):
			t = self.computeTimeIntervals(x)

		C = self.pointInterpolation(x, t)		# determine C
		
		return self.splineGeneration(C, t, tn)
		

## ================================================================================================ ##
## 												TESTING 											##
## ================================================================================================ ##

### CoM linear path ###
t_com = np.array([0., 14., 28., 42., 56.]) 
x_com = np.array([0.0, 0.0, 0.0])
x_com = np.vstack((x_com, np.array([0.5, 0.0, 0.0]) ))
x_com = np.vstack((x_com, np.array([1.0, 0.0, 0.0]) ))
x_com = np.vstack((x_com, np.array([1.4, 0.0, 0.0]) ))
x_com = np.vstack((x_com, np.array([1.8, 0.0, 0.0]) ))
# x_com = np.vstack((x_com, np.array() ))

spliner = SplineGenerator()
x_out  = spliner.generateSpline(x_com, t_com, 0.01)
# print xout[0]
# Plot.plot_2d(x_out[0],x_out[1])