#include  "ros/ros.h"
#include <trajectory_generator/bspline_generator.h>
#include <trajectory_generator/polynomial.h>

#include <iostream>

int main(int argc, char** argv) {

  ros::init(argc, argv, "polynomial_node");

  ROS_INFO("Initiated!");
  ros::NodeHandle n;

  Polynomial planner(n);

  ros::AsyncSpinner spinner(1); // Use 1 thread
	spinner.start();
	ros::waitForShutdown();

  return 0;
}