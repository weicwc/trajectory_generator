from setuptools import setup

setup(
    version='0.0.0',
    packages=['trajectory_generator'],
    package_dir={'': 'src'}
)