%% inputs
% --- variables
ux = 0.0;
uy = 0.1;
phi_dot = 0;
theta_dot_J1 = 0; 
theta_dot_J2 = 0;
theta_J1 = 1*pi/4;  % strictly these two are dependant on theta_dot
theta_J2 = 0*pi/4;
% --- constants
r = 0.125/2; % wheel radius
gamma = [pi/4 -pi/4 pi/4 -pi/4]; % roller angle
l2 = [-0.25 -0.25 0.25 0.25];   % body to joint frame linear transform in X_B
l3 = [0.4 0.1 0.1 0.4];     % joint to wheel linear transform in X_J
% --- build vectors
theta_dot = [theta_dot_J1 theta_dot_J1 theta_dot_J2 theta_dot_J2];
theta = [theta_J1 theta_J1 theta_J2 theta_J2];

%% wheel positions
% --- from XbYb
xw_b = -l3.*sin(theta) + l2;
yw_b = +l3.*cos(theta);
% from XRYR to XBYB
X_RB = -mean(xw_b);
Y_RB = -mean(yw_b);
% --- from XRYR
xw_R = xw_b + X_RB;
yw_R = yw_b + Y_RB;


%% Build matricies and solve
for i = 1:4
    cmd = [ux uy phi_dot theta_dot(i)]';
    W = [r, cos(gamma(i)); 0,  sin(gamma(i))];
    T = [cos(theta(i)), -sin(theta(i)); sin(theta(i)), cos(theta(i))];
    J_theta = [-l3(i)*cos(theta(i)); -l3(i)*sin(theta(i))];
    J_phi = [-yw_R(i); xw_R(i)];
    Jxy = [1 0; 0 1];
    J = [Jxy J_phi J_theta];
    % --- wheel vel array
    Omega_up(:,i) = inv(W)*inv(T)*J*cmd;
    % --- extras for checks and plots
    Up_R(:,i) = J*cmd;
    Up_R_check(:,i) = T*W*Omega_up(:,i);
    Up_d(:,i) = [Omega_up(1,i)*r; 0];
    Up_d_R(:,i) = T*Up_d(:,i);
    Up_p(:,i) = [Omega_up(2,i).*cos(gamma(i)); Omega_up(2,i).*sin(gamma(i))];
    Up_p_R(:,i) = T*Up_p(:,i);
end