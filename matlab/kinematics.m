close all;
clear all;

x_baselink=0; y_baselink=0;

PI= 3.14159;

% Joint angle / leg angle - vary these values
alpha_j1 = PI/4;
beta_j2  = PI/4;

%--------------------------

% Distance from base_link to joint 1 and 2 in reference frame of base_link
L2_1_x=-0.216;
L2_1_y= 0;
L2_2_x= 0.216;
L2_2_y= 0;


% Distance from joint 1 to wheels 1 and 2 in reference frame of joint 1
L3_w1 = -0.1215;
L3_w2 = -0.4005;

% Distance from joint 2 to wheels 3 and 4 in reference frame of joint 2
L3_w3 = 0.1215;
L3_w4 = 0.4005;

%---- Calculate wheel position for a given joint angle
[x_wheel_w1,y_wheel_w1] = calc_wheel_x_y(L3_w1, alpha_j1);
[x_wheel_w2,y_wheel_w2] = calc_wheel_x_y(L3_w2, alpha_j1);
[x_wheel_w3,y_wheel_w3] = calc_wheel_x_y(L3_w3, beta_j2);
[x_wheel_w4,y_wheel_w4] = calc_wheel_x_y(L3_w4, beta_j2);


%----------------------- Calculate position of midpoint -----------------
[L1_x, L1_y] = calc_L1_x_y(x_baselink,  L3_w1, L3_w2, L3_w3, L3_w4, alpha_j1, beta_j2 ) ;

% Check using only wheel positions
[L1_x_check, L1_y_check] = calc_L1_using_wheels( L2_1_x + x_wheel_w1, y_wheel_w1, L2_1_x + x_wheel_w2 , y_wheel_w2, L2_2_x + x_wheel_w3 , y_wheel_w3, L2_2_x + x_wheel_w4, y_wheel_w4);

% -------------------- Plot joints and wheels -----------------

figure
hold on;
grid on;
%pbaspect([1 1]);
xlim([-1 1])
ylim([-1 1])

plot(x_baselink , y_baselink,'o-', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'black', 'HandleVisibility','off')
plot(L2_1_x , y_baselink,'o-', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'blue', 'HandleVisibility','off')
plot(L2_2_x , y_baselink,'o-', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'blue', 'HandleVisibility','off')

plot(L2_1_x + x_wheel_w1 , y_wheel_w1, 's-', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', [1 .6 .6], 'HandleVisibility','off')
plot(L2_1_x + x_wheel_w2 , y_wheel_w2, 's-', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', [1 .6 .6], 'HandleVisibility','off')
plot(L2_2_x + x_wheel_w3 , y_wheel_w3, 's-', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', [1 .6 .6], 'HandleVisibility','off')
plot(L2_2_x + x_wheel_w4 , y_wheel_w4, 's-', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', [1 .6 .6], 'HandleVisibility','off')


% --------------- Plot L1 -----------------

plot(L1_x, L1_y, 'o', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'red', 'DisplayName','L1', 'HandleVisibility','on')
plot(L1_x_check, L1_y_check, '*', 'MarkerSize', 10, 'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'red', 'DisplayName','L1', 'HandleVisibility','on')

% -------------------- Plot lines for legs---------------

j1_x_range = linspace(x_baselink, L2_1_x); j1_y_range = linspace(y_baselink, L2_1_y);
j2_x_range = linspace(x_baselink, L2_2_x); j2_y_range = linspace(y_baselink, L2_2_y);

x_range_wheel_w1 = linspace( L2_1_x, L2_1_x + x_wheel_w1) ; y_range_wheel_w1 = linspace( L2_1_y, L2_1_y + y_wheel_w1);
x_range_wheel_w2 = linspace( L2_1_x, L2_1_x + x_wheel_w2) ; y_range_wheel_w2 = linspace( L2_1_y, L2_1_y + y_wheel_w2);
x_range_wheel_w3 = linspace( L2_2_x, L2_2_x + x_wheel_w3) ; y_range_wheel_w3 = linspace( L2_2_y, L2_2_y + y_wheel_w3);
x_range_wheel_w4 = linspace( L2_2_x, L2_2_x + x_wheel_w4) ; y_range_wheel_w4 = linspace( L2_2_y, L2_2_y + y_wheel_w4);

plot(j1_x_range,j2_y_range, 'k-', 'HandleVisibility','off')
plot(j2_x_range,j2_y_range, 'k-', 'HandleVisibility','off')
plot(x_range_wheel_w1, y_range_wheel_w1, 'k-', 'HandleVisibility','off')
plot(x_range_wheel_w2, y_range_wheel_w2, 'k-', 'HandleVisibility','off')
plot(x_range_wheel_w3, y_range_wheel_w3, 'k-', 'HandleVisibility','off')
plot(x_range_wheel_w4, y_range_wheel_w4, 'k-', 'HandleVisibility','off')


legend('L1', 'L1 check')

%----------------- Functions -------------

function [x_wheel, y_wheel] = calc_wheel_x_y(L3_wheel,input_angle)
x_wheel = L3_wheel*cos(input_angle);
y_wheel = L3_wheel*sin(input_angle);
end

function [L1_x, L1_y] = calc_L1_x_y(x_baselink,  L3_w1, L3_w2, L3_w3, L3_w4, alpha, beta )  
L1_x =  x_baselink;

L1_j1 = (L3_w2*sin(alpha) ) + 0.5* (L3_w1*sin(alpha) - L3_w2*sin(alpha));
L1_j2 = (L3_w3*sin(beta) ) + 0.5* (L3_w4*sin(beta) - L3_w3*sin(beta));

L1_y = 0.5 * (L1_j1 + L1_j2);

end

function [L1_x, L1_y] = calc_L1_using_wheels( x_w1, y_w1, x_w2, y_w2, x_w3, y_w3, x_w4, y_w4)
L1_x = 0.25 * (x_w1 + x_w2 + x_w3 + x_w4);
L1_y = 0.25 * (y_w1 + y_w2 + y_w3 + y_w4);
end