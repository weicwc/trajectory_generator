#define _USE_MATH_DEFINES   //for PI etc
#ifndef BSPLINE_GENERATOR_H_
#define BSPLINE_GENERATOR_H_

#include <iostream>
#include <Eigen/Dense>
#include <math.h> 
#include <queue>

struct Point {
  Point(int dim){ 
		pos.resize(dim);
		vel.resize(dim);
		acc.resize(dim);
		setZero(); 
	}

  Eigen::VectorXd pos;
  Eigen::VectorXd vel;
  Eigen::VectorXd acc;
  double t;

  void setZero()
  {
    pos.setZero();
    vel.setZero();
    acc.setZero();
		t = 0;
  }
};

struct Trajectory{
  Trajectory() {}

  std::vector<Point> points;					// vector of points
	std::vector<std::string> axis_name;	// name of axis
	
	double getMaxTime()
	{
		return ( points[points.size()-1].t );
	}
	double getSize()
	{
		return ( points.size() );
	}
	void clear()
	{
		points.clear();
		axis_name.clear();
	}
};

class BSplineGenerator{
 public:
  BSplineGenerator();
	~BSplineGenerator();

	// BSpline functions
	void knotFunction();
	int whichSpan(double u);
	void DersBasisFunction(double u, int i, int n, Eigen::MatrixXd& derv);
	void controlPoint();
	
	// Add intermediate points
	void pointInterpolation(const std::vector<Eigen::VectorXd>& waypoints);
	
	// Samples the entire trajectory
	void sampleEntireTrajectory(const double ts, Trajectory* trajectory);

	// 	computes b-spline given via points and time interval
	bool generateSpline(const std::vector<Eigen::VectorXd>& waypoints,
											const Eigen::VectorXd& tseg, const double ts,
											Trajectory* trajectory);
	
	// Function to test spline output
	void test();
 
 private:
  int s_order_;		// order of spline
	int s_degree_; 	// derivate degree
	int s_dim_;			// spline dimension
  int n_knot_;		// number of knots

	Eigen::MatrixXd C_;	// via points
	Eigen::MatrixXd P_; // control points
	Eigen::VectorXd t_;	// time array
	Eigen::VectorXd U_;	// something?
};

#endif  // BSPLINE_GENERATOR_H_
