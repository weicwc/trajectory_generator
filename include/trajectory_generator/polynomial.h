#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H

#include <iostream>
#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <Eigen/Dense>
#include <eigen_conversions/eigen_msg.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <visualization_msgs/MarkerArray.h>
#include <nav_msgs/Path.h>
#include <rviz_visual_tools/rviz_visual_tools.h>

#include "trajectory_generator/bspline_generator.h"
#include "mirrax_controller/kinematics.h"

struct TrajectorySampler
{
  TrajectorySampler()
  {
    dt_ = 0.01;
    current_sample_time_ = 0.0;
    current_time_index_ = 0;
  }
  double dt_;
  double current_sample_time_;
  int current_time_index_;
  bool final_trigger_;
};

class Polynomial {
 public:
  Polynomial(ros::NodeHandle& nh);
  ~Polynomial();

  // Constrain speed (Not used atm)
  void setMaxSpeed(const double max_v, const double max_w);
  
  // Callback receiving waypoints. On receiving, generate spline 
  // and start publishing trajectory points at fixed intervals  
  void waypointCallback(const trajectory_msgs::JointTrajectory::ConstPtr& msg);

  // Estimate segment times by decoupling trajectory into 3 different parts,
  // (1) xy (2) yaw (3) leg_joints; estimate the segment time for each part,
  // then take the maximum. Time estimate based on mav_trajectory_generation
  // using the magic_fabian_constant
  void estimateSegmentTimes(const std::vector<Eigen::VectorXd>& waypoints, Eigen::VectorXd& tseg);

  // Check trajectory feasibiliy SPECIFICALLY for mirrax
  // TODO: not done
  bool checkFeasibility(const Trajectory& trajectory);
  
  // Generates the trajectory using BSpline approach
  bool planTrajectory(const std::vector<Eigen::VectorXd>& waypoints, 
                      const Eigen::VectorXd tseg, 
                      Trajectory* trajectory);
  // TEMP: for experiment
  // void checkRange(const mav_trajectory_generation::Trajectory& trajectory);

  // Sample trajectory
  void commandTimerCallback(const ros::TimerEvent&);
  void trajectoryToMsg(int sample_time, trajectory_msgs::MultiDOFJointTrajectory& msg);

  // Visualize the trajectory
  bool publishVisualTrajectory();
  bool visualizeTrajectory(double distance, std::string frame_id,
                           visualization_msgs::MarkerArray* markers);
  // Clears markers in RViZ (does not work! F***)
  bool clearMarkers();

 private:
  ros::Publisher pub_markers_;
  ros::Publisher pub_path_;
  ros::Publisher pub_trajectory_;
  ros::Subscriber sub_waypoints_;

  ros::NodeHandle& nh_;
  
  double max_v_;      // m/s  , linear velocity constraint
  double max_a_;      // m/s^2, linear acceleration constraint
  double max_ang_v_;  // rad/s  , yaw velocity constraint
  double max_ang_a_;  // rad/s^2, yaw acceleration constraint  
  double max_j_v_;    // joint velocity constraint
  double max_j_a_;    // joint acceleration constraint
  double z_offset_;

  mirrax::Kinematics kin_;
  std::string robot_name_;
  double max_wheel_velocity_;

  ros::Timer publish_timer_;
  ros::Time start_time_;
  ros::Publisher pub_command_;

  Trajectory trajectory_;
  TrajectorySampler traj_sampler_;

};

#endif // POLYNOMIAL_H
